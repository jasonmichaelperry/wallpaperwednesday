//
//  WWServiceLoader.h
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/26/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <netinet/in.h>
#import "WWServiceLoaderDelegatee.h"
#import "WWWallpaperItem.h"

@interface WWServiceLoader : NSObject
{
    //id <WWServiceLoaderDelegatee> delegate;
    
    //
    NSURL *serviceLocation;
}

//
@property(nonatomic,weak) id <WWServiceLoaderDelegatee> delegate;

//
-(void)loadFrom: (NSInteger) start withRange: (NSInteger) range;

//
+ (BOOL) canAccessService;
+ (NSMutableArray *) deseralizeCurrentWallpapers;
+ (void) serializeCurrentWallpapers: (NSArray *) theWallpapers;
+ (NSURL *) saveImage: (NSURL *) remotePath notDeleting: (NSArray *) theWallpapers;
+ (NSImage*) resizeImage:(NSImage*)sourceImage size:(NSSize)size;
+ (void) addAppAsLoginItem;
+ (void) deleteAppFromLoginItem;

@end
