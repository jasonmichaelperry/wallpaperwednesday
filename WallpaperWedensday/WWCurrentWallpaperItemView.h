//
//  WWCurrentWallpaperItemView.h
//  WallpaperWednesday
//
//  Created by Jason Michael Perry on 10/30/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WWServiceLoader.h"
#import "WWWallpaperItem.h"

@interface WWCurrentWallpaperItemView : NSView
{
    //
    WWWallpaperItem* wallpaperItem;
    
    //
    NSTextField *boxLabel;
    NSImageView *previewImage;
    NSTextField *titleLabel;
    NSTextField *artistLabel;
}

//
@property(nonatomic,retain) WWWallpaperItem* wallpaperItem;

@end
