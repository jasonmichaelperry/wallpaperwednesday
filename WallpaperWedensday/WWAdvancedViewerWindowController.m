//
//  WWAdvancedViewerWindowController.m
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/27/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import "WWAdvancedViewerWindowController.h"

@interface WWAdvancedViewerWindowController ()

@end

@implementation WWAdvancedViewerWindowController

//
@synthesize imageBrowser, zoomSlider, delegate;

/**** INITILIZATION *****/

//
- (id)init
{
    //
    self = [super initWithWindowNibName:@"AdvancedViewerWindow" owner: self];
    
    //
    if (self) {
        // initializations
    }
    
    //
    return self;
}

//
- (id)initWithWindow:(NSWindow *)window
{
    //
    self = [super initWithWindow:window];
    
    //
    if (self)
    {
        // Initialization code here.
    }
    
    //
    return self;
}

//
- (void)windowDidLoad
{
    //
    [super windowDidLoad];
    
    //
    [zoomSlider setMinValue: 0];
    [zoomSlider setMaxValue: 1];
    [zoomSlider setFloatValue: 0.5];
    
    //
    [imageBrowser setDelegate: self];
    [imageBrowser setDataSource: self];
    [imageBrowser setCanControlQuickLookPanel: YES];
    [imageBrowser setIntercellSpacing: NSMakeSize( 10, 10)];
    [imageBrowser setZoomValue: [zoomSlider floatValue]];
    [imageBrowser setConstrainsToOriginalSize: YES];
    [imageBrowser setCellsStyleMask: IKCellsStyleShadowed|IKCellsStyleTitled|IKCellsStyleSubtitled];
    
    //
    [imageBrowser reloadData];
}

/**** INTERFACE ACTIONS *****/

//
-(void) doSliderMove: (id) slider
{
    //
    [imageBrowser setZoomValue: [zoomSlider floatValue]];
}

/**** DATA ACTIONS *****/

//
-(void) setWallpapers: (NSArray *) theWallpapers
{
    //pass the contents into this app
    wallpapers = theWallpapers;
    
    //
    [imageBrowser reloadData];
}

/**** IMAGE KIT ACTIONS *****/

//
- (int) numberOfItemsInImageBrowser: (IKImageBrowserView *) view
{
    return (int)[wallpapers count];
}

//
- (id) imageBrowser: (IKImageBrowserView *) view itemAtIndex: (int) index
{
    return [wallpapers objectAtIndex:index];
}

//
- (void) imageBrowser:(IKImageBrowserView *) aBrowser cellWasDoubleClickedAtIndex:(NSUInteger) index
{    
    //
    [delegate selectWallpaper: [wallpapers objectAtIndex: index]];
}

@end
