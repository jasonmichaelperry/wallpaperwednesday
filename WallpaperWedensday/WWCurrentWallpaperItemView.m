//
//  WWCurrentWallpaperItemView.m
//  WallpaperWednesday
//
//  Created by Jason Michael Perry on 10/30/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import "WWCurrentWallpaperItemView.h"

@implementation WWCurrentWallpaperItemView

//
@synthesize wallpaperItem;

//
- (id)initWithFrame:(NSRect)frame
{
    //
    self = [super initWithFrame:frame];
    
    //
    if (self)
    {
        //
        boxLabel = [[NSTextField alloc] initWithFrame: NSMakeRect(25, 60, frame.size.width-25, 14)];
        [boxLabel setStringValue: @"Current Wallpaper:"];
        [boxLabel setBezeled:NO];
        [boxLabel setDrawsBackground:NO];
        [boxLabel setEditable:NO];
        [boxLabel setSelectable:NO];
        [boxLabel setFont: [NSFont systemFontOfSize: 11.0f]];
        
        //
        previewImage = [[NSImageView alloc] initWithFrame: NSMakeRect(25, 5, 45, 30)];
        
        //
        titleLabel = [[NSTextField alloc] initWithFrame: NSMakeRect(75, 20, frame.size.width-90, 15)];
        //[titleLabel setFont: [NSFont systemFontOfSize: 14.0f]];
        [titleLabel setBezeled:NO];
        [titleLabel setDrawsBackground:NO];
        [titleLabel setEditable:NO];
        [titleLabel setSelectable:NO];
        
        //
        artistLabel = [[NSTextField alloc] initWithFrame: NSMakeRect(75, 5, frame.size.width-90, 15)];
        [artistLabel setBezeled:NO];
        [artistLabel setDrawsBackground:NO];
        [artistLabel setEditable:NO];
        [artistLabel setSelectable:NO];
        
        //
        //[self addSubview: boxLabel];
        [self addSubview: previewImage];
        [self addSubview: titleLabel];
        [self addSubview: artistLabel];
    }
    
    //
    return self;
}

//
- (void) setWallpaperItem:(WWWallpaperItem *)theWallpaperItem
{
    //
    wallpaperItem = theWallpaperItem;
    
    //
    NSString *nameLabelText = (theWallpaperItem == nil) ? @"None" : [wallpaperItem name];
    NSString *artistLabelText = (theWallpaperItem == nil) ? @"None" : [wallpaperItem author];
 
    //
    NSImage *previewImg = [[NSImage alloc] initWithContentsOfURL: [wallpaperItem localPath]];
    
    //
    [previewImage setImage: [WWServiceLoader resizeImage: previewImg size: NSMakeSize(45, 30)]];
    [titleLabel setStringValue: nameLabelText];
    [artistLabel setStringValue: artistLabelText];
}

//
- (void) mouseDown:(NSEvent *)theEvent
{
    //launch the artist information in a browser
    [[NSWorkspace sharedWorkspace] openURL: [wallpaperItem webURL]];
}

@end
