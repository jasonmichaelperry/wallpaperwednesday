//
//  WWWallpaperItemView.m
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/27/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import "WWWallpaperItemView.h"

@implementation WWWallpaperItemView

//
@synthesize delegate, wallpaperItem;

/**** INITILIZATION *****/

//
- (id)initWithFrame:(NSRect)frame
{
    //
    self = [super initWithFrame:frame];
    
    //
    if (self)
    {
        
        //
        previewImage = [[NSButton alloc] initWithFrame: NSMakeRect(0, 0, 150, 100)];
        
        //
        [previewImage setTitle: @""];
        [previewImage setBordered: NO];
        [previewImage setTarget: self];
        [previewImage setHidden: YES];
        [previewImage setAction: @selector(doSelect:)];
        [(NSButtonCell *)[previewImage cell] setImageDimsWhenDisabled: NO];
        
        //
        [self addSubview: previewImage];
    }
    //
    return self;
}

//
- (void)drawRect:(NSRect)dirtyRect
{
    //
	[super drawRect:dirtyRect];
	
    // Drawing code here.
}

/**** INITILIZATION *****/

//
- (void) setEnabled:(BOOL)isEnabled
{
    [previewImage setEnabled: isEnabled];

}

//
- (void) doSelect: (NSImageView *) imageView
{
    //set the wallpaper to this item
    [delegate selectWallpaper: wallpaperItem];
}

//
- (void) displayWithWallpaperItem: (WWWallpaperItem *) theWallpaperItem
{
    //
    [self setWallpaperItem: theWallpaperItem];
    
    //
    NSImage *imageToDisplay = [[NSImage alloc] initWithContentsOfURL: [theWallpaperItem previewImageURL]];
    [imageToDisplay setScalesWhenResized: YES];
    
    //
    NSImage *resizedImage = [WWServiceLoader resizeImage: imageToDisplay size: NSMakeSize(150, 100)];
    
    
    //
    [previewImage setHidden: NO];
    [previewImage setAlternateImage: resizedImage];
    [previewImage setImage: resizedImage];

}

@end
