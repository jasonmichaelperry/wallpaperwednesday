//
//  WWAppDelegate.m
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/26/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import "WWAppDelegate.h"

@implementation WWAppDelegate

@synthesize preferencesController, advancedViewerController;

/**** APPLICATION INITILIZATION *****/

//
@synthesize window;


//
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //timeout duration used if internet connection is not available
    timeout = 15.0f;
    
    //creates an array to store all the current wallper items
    currentWallpapers = [[NSMutableArray alloc] init];
    
    //if this is the first run initiate some values
    [self isFirstRun];
    
    //listen for the screen changing to a new one
    [[[NSWorkspace sharedWorkspace] notificationCenter] addObserver: self selector: @selector(spaceChanged:) name: NSWorkspaceActiveSpaceDidChangeNotification object: nil];
    
    //retreives the current listing of wallpaper items from storage
    currentWallpapers = [WWServiceLoader deseralizeCurrentWallpapers];
    
    //force a space change to render the item for this display
    [self spaceChanged: nil];
    
    //
    //[self loadWallpapers];
    [NSThread detachNewThreadSelector:@selector(loadWallpapers) toTarget:self withObject:nil];
    
    //builds the menu based interface for the application
    [self buildMenuList];
    [self activateStatusMenu];
}

//Atempts to determine if this app has run before
- (void) isFirstRun
{
    //allows access to any default for this user
    NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
    
    //get back a required property
    NSString *intervalValue = (NSString *)[userSettings objectForKey: @"wwp_interval"];
    
    //if the value is nil this app has never run before so lets set the defaults
    if( intervalValue == nil )
    {
        //defaults applied for all elements
        [userSettings setObject: @"6" forKey: @"wwp_interval"];
        [userSettings setObject: @"0" forKey: @"wwp_autoStart"];
    }

}

/**** HANDLES WALLPAPER TIMER *****/

//
- (void) startTimer
{
    //
    NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
    
    //retreive the interval to alter the wallpaper image
    NSString *intervlValue = [userSettings objectForKey: @"wwp_interval"];
    
    //is it never?
    if( [intervlValue integerValue] != 6 ) [self startTimerForInterval: [intervlValue integerValue]];
}

//
- (void) startTimerForInterval: (NSInteger) interval
{
    //
    timedServices = [[WWTimedService alloc] init];
    [timedServices setDelegate: self];
    
    //
    NSTimeInterval intervalToRun;
    
    //
    switch ( interval )
    {
        case 1:
            intervalToRun = (60*15);
            break;
            
        case 2:
            intervalToRun = (60*30);
            break;
            
        case 3:
            intervalToRun = (60*60);
            break;
            
        case 4:
            intervalToRun = (60*60*24);
            break;
            
        case 5:
            intervalToRun = (60*60*24*7);
            break;
    }
    
    //
    [timedServices startTimerWithInterval: intervalToRun];
}

//
- (void) stopTimer
{
    //possible the object was never created
    if( timedServices == nil ) return;
    
    //if exists cancel the previous timer
    [timedServices stopTimer];
}


/**** LOADS WALLPAPERS *****/

//
- (void) loadWallpapers
{
    //testing appears to show that this fails if an invalid ethernet connection is available
    NSLog( @"Can access the Internet: %i", [WWServiceLoader canAccessService] );
    
    //if we do not have an internet connection try and load them again after a duration of time has elapsed
    if( [WWServiceLoader canAccessService] == NO )
    {
        //testing duration growth for timeout
        NSLog( @"Will attempt to connect again in %f seconds", timeout );
        
        //wait 5 seconds
        [NSThread sleepForTimeInterval: timeout];
        
        //increment the timeout by 15 seconds each attempt and cap out at 60*60 which is an hour
        timeout = (timeout < 3600) ? timeout+15.0f : 3600.0f;
        
        //try again
        [NSThread detachNewThreadSelector:@selector(loadWallpapers) toTarget:self withObject:nil];
        
        //kill this thread
        return;
    }
    
    //reseed the timeout for next time
    timeout = 15.0f;
    
    //activates a service call to load wallpaper options available
    WWServiceLoader *service = [[WWServiceLoader alloc] init];
    [service setDelegate: self];
    [service loadFrom:0 withRange:100];
}

/**** MENU INITILIZATION *****/

//
- (void)activateStatusMenu
{
    //
    NSStatusBar *bar = [NSStatusBar systemStatusBar];
    
    //
    statusItem = [bar statusItemWithLength:NSVariableStatusItemLength];

    //
    //[statusItem setTitle: NSLocalizedString(@"WW",@"")];
    [statusItem setImage: [NSImage imageNamed: @"MenuIcon"]];
    [statusItem setHighlightMode:YES];
    [statusItem setMenu: menu];
}

//
- (void)buildMenuList
{
    //
    menu = [[NSMenu alloc] init];
        
    //create a view to display wallpaper options and observe it
    wallpaperMenuView = [[WWWallpaperMenu alloc] initWithFrame: NSMakeRect(0, -10, 200, 100)];
    [wallpaperMenuView setDelegate: self];
    
    //
    currentWallpaperView = [[WWCurrentWallpaperItemView alloc] initWithFrame: NSMakeRect(0, 0, 200, 40)];
    [currentWallpaperView setWallpaperItem: currentWallpaper];
    
    //
    NSMenuItem *wallpaperMenuItem = [[NSMenuItem alloc] initWithTitle: @"" action: nil keyEquivalent: @""];
    [wallpaperMenuItem setView: wallpaperMenuView];  //uses this view instead of the menu item

    //
    NSMenuItem *advancedViewerMenuItem = [[NSMenuItem alloc] initWithTitle: @"Browse Wallpapers..." action:@selector(showAdvancedViewer:) keyEquivalent: @""];
    
    //
    currentWallpaperMenuItem = [[NSMenuItem alloc] initWithTitle: @"" action: nil keyEquivalent: @""];
    [currentWallpaperMenuItem setView: currentWallpaperView];
    
    //
    NSMenuItem *prefencesMenuItem = [[NSMenuItem alloc] initWithTitle: @"Preferences" action:@selector(showPreferences:) keyEquivalent: @""];
    
    //
    NSMenuItem *quitMenuItem = [[NSMenuItem alloc] initWithTitle: @"Quit" action:@selector(doQuit:) keyEquivalent: @""];

    //
    [menu addItem: currentWallpaperMenuItem];
    [menu addItem: [NSMenuItem separatorItem]];
    [menu addItem: wallpaperMenuItem];
    [menu addItem: [NSMenuItem separatorItem]];
    [menu addItem: advancedViewerMenuItem];
    [menu addItem: [NSMenuItem separatorItem]];
    [menu addItem: prefencesMenuItem];
    [menu addItem: [NSMenuItem separatorItem]];
    [menu addItem: quitMenuItem];    
}

/**** WALLPAPER MENU ITEM DELEGATES *****/

//
- (void) selectRandomWallpaper
{
    //finds a random wallpaper item from the array to display
    NSLog( @"Time to change the wallpaer!" );
    
    //no internet connection or no wallpapers are loaded
    if( [wallpapers count] == 0 || [WWServiceLoader canAccessService] == NO )
        return;
    
    //hopefully generates a random number between the high and the low
    int low = 0;
    int high = (int)[wallpapers count];
    int rnd = low + arc4random() % (high - low);

    //gets the random item from the array and applies it
    WWWallpaperItem *wi = [wallpapers objectAtIndex: rnd];
    
    //Applies wallpaper as a thread to prevent interface from freezing
    [NSThread detachNewThreadSelector:@selector(applyWallpaper:) toTarget:self withObject: wi];
}

//
- (void) selectWallpaper:(WWWallpaperItem *)theWallpaper
{
    //Applies wallpaper as a thread to prevent interface from freezing
    [NSThread detachNewThreadSelector:@selector(applyWallpaper:) toTarget:self withObject:theWallpaper];
    
    //
    [menu cancelTracking];
}

//
- (void) applyWallpaper:(WWWallpaperItem *)theWallpaper
{
    //testing appears to show that this fails if an invalid ethernet connection is available
    NSLog( @"Can download image from the Internet: %i", [WWServiceLoader canAccessService] );
    
    //if we do not have an internet connection try and apply the image at a later time
    if( [WWServiceLoader canAccessService] == NO )
    {
        //testing duration growth for timeout
        NSLog( @"Will attempt to connect again in %f seconds", timeout );
        
        //wait 5 seconds
        [NSThread sleepForTimeInterval: timeout];
        
        //increment the timeout by 15 seconds each attempt and cap out at 60*60 which is an hour
        timeout = (timeout < 3600) ? timeout+15.0f : 3600.0f;
        
        //try again
        [NSThread detachNewThreadSelector:@selector(applyWallpaper:) toTarget:self withObject:theWallpaper];
        
        //kill this thread
        return;
    }
    
    //resed the timeout
    timeout = 15.0f;
    
    //reference the remote path
    NSURL *imageUrl = [theWallpaper imageURL];
    
    //save the file to the local machine and pass a local reference to the asset
    NSURL *localPath = [WWServiceLoader saveImage: imageUrl notDeleting: currentWallpapers];
    
    //did we get a valid local path?
    if( localPath == nil )
    {
        //
        [NSApp activateIgnoringOtherApps:YES];
        
        //
        NSRunAlertPanel( @"Could not download wallpaper", @"Wallpaper asset failed to download.  This could be an issue with your interent connection or the wallpaper is no longer available.", @"OK", nil, nil);
        
        //stop progress here
        return;
    }
    
    //save the local path of the image for later usage
    [theWallpaper setLocalPath: localPath];
    
    //stores an error if it should occur
    NSError *error;
    
    //determines what should be the current screen
    NSScreen *currentScreen = [NSScreen mainScreen];
    
    //determine the location for the current url
    NSURL *currentLocalPath = [[NSWorkspace sharedWorkspace] desktopImageURLForScreen: currentScreen];
    
    //loop through and determine if the collection has this item already
    for( int i = 0; i < [currentWallpapers count]; i++ )
    {
        //determines if this is or was a previous background
        if( [[currentLocalPath lastPathComponent] isEqualToString: [[(WWWallpaperItem *)[currentWallpapers objectAtIndex: i] localPath] lastPathComponent] ] )
        {
            //purge it from the array
            //[currentWallpapers removeObjectAtIndex: i];
            
            //if it was
            break;
        }
    }
    
    //apply the selected wallpaper as the background for this computer
    [[NSWorkspace sharedWorkspace] setDesktopImageURL: localPath forScreen: currentScreen options: nil error:&error];
    
    //display any error messages
    if( error )
    {
        //
        [NSApp activateIgnoringOtherApps:YES];
        
        //
        NSRunAlertPanel( @"Setting background image failed", @"", @"OK", nil, nil);
        
        //provide feedback on the error
        //NSLog( [error description] );
        
        //stop progress here
        return;
    }
    
    //store it in the database
    [currentWallpapers addObject: theWallpaper];
    
    //
    [currentWallpaperView setWallpaperItem: theWallpaper];
    
    //serialized the current wallpapers
    [WWServiceLoader serializeCurrentWallpapers: currentWallpapers];
    
    //
    [self showNotification];
}

//
- (void) reloadWallpapers
{
    //activates a service call to load wallpaper options available
    [NSThread detachNewThreadSelector:@selector(loadWallpapers) toTarget:self withObject:nil];
}

/**** SERVICE DELEGATES *****/

//
-(void)loadCompleteWithData:(NSArray *)theWallpapers fromService:(id)theService
{
    //set the wallpapers array with items retreived
    wallpapers = theWallpapers;
    
    //place these wallpapers into the wallpaper menu item view to display
    [wallpaperMenuView setWallpapers: wallpapers];
    
    //
    [self startTimer];
}

//
-(void)loadDidFailWithError:(NSError *)error
{
    //
    [NSApp activateIgnoringOtherApps:YES];

    //
    NSRunAlertPanel( @"Loading of wallpapers failed", @"Loading failed.  This could be do to a temporary server issue or lack of an Internet connection.  Please hit the retry button to attempt to reload wallpaper assets.", @"OK", nil, nil);
}

/**** PREFERENCES DELEGATES *****/

//
- (void) preferencesDidChange
{    
    //
    [self stopTimer];
    
    //
    [self startTimer];
}


/**** SPACE CHANGE EVENTS *****/

//
- (void) spaceChanged: (NSNotification *) note
{
    //
    BOOL found = NO;
    
    //determines what should be the current screen
    NSScreen *currentScreen = [NSScreen mainScreen];
    
    //apply the selected wallpaper as the background for this computer
    NSURL *currentLocalPath = [[NSWorkspace sharedWorkspace] desktopImageURLForScreen: currentScreen];
    
    //
    //NSLog( @"%@", [currentLocalPath description]);
    
    //update current image shown using that as the uid
    //loop through and determine if the collection has this item already
    for( int i = 0; i < [currentWallpapers count]; i++ )
    {
        //determines if this is or was a previous background
        if( [[currentLocalPath lastPathComponent] isEqualToString: [[(WWWallpaperItem *)[currentWallpapers objectAtIndex: i] localPath] lastPathComponent] ] )
        {
            //get the wallpaper for this screen
            currentWallpaper = [currentWallpapers objectAtIndex: i];
            
            //
            found = YES;
            
            //if it was
            break;
        }
    }
 
    //if we did not find the wallpaper to match this item
    if( !found )
        currentWallpaper = nil;
    
    //update the display to show this item
    [currentWallpaperView setWallpaperItem: currentWallpaper];
}

/**** NOTIFICATIONS *****/

//
- (void) showNotification
{
    NSUserNotification *notification = [[NSUserNotification alloc] init];
    notification.title = @"New Wallpaper!!";
    notification.informativeText = @"A new wallpapper has been applied";
    notification.soundName = NSUserNotificationDefaultSoundName;
    
    [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification: notification];
}


/**** MENU ACTIONS *****/

//displays an advanced viewer with a tile display of all items
-(void)showAdvancedViewer: (id) menuItem
{
    //
    if( [[advancedViewerController window] isVisible] ) return;
    
    //
    [NSApp activateIgnoringOtherApps:YES];
    
    //create an instance of a prefference window controller from the xib
    advancedViewerController = [[WWAdvancedViewerWindowController alloc] init];
    
    //populate the view with a collection of all loaded wallpapers
    [advancedViewerController setWallpapers: wallpapers];
    
    //
    CGFloat xPos = NSWidth([[[advancedViewerController window] screen] frame])/2 - NSWidth([[advancedViewerController window] frame])/2;
    CGFloat yPos = NSHeight([[[advancedViewerController window] screen] frame])/2 - NSHeight([[advancedViewerController window] frame])/2;
    [[advancedViewerController window] setFrame:NSMakeRect(xPos, yPos, NSWidth([[advancedViewerController window] frame]), NSHeight([[advancedViewerController window] frame])) display: NO];

    //show the window to the user
    [advancedViewerController showWindow: self];
    [advancedViewerController setDelegate: self];
    
    //
    [[advancedViewerController window] makeKeyWindow];
}


//preferences for all options to customize this app
-(void)showPreferences: (id) menuItem
{
    //
    if( [[preferencesController window] isVisible] ) return;
    
    //
    [NSApp activateIgnoringOtherApps:YES];
    
    //create an instance of a prefference window controller from the xib
    preferencesController = [[WWPreferencesWindowController alloc] init];
    
    //
    CGFloat xPos = NSWidth([[[preferencesController window] screen] frame])/2 - NSWidth([[preferencesController window] frame])/2;
    CGFloat yPos = NSHeight([[[preferencesController window] screen] frame])/2 - NSHeight([[preferencesController window] frame])/2;
    [[preferencesController window] setFrame:NSMakeRect(xPos, yPos, NSWidth([[preferencesController window] frame]), NSHeight([[preferencesController window] frame])) display: NO];
    
    //show the window to the user
    [preferencesController showWindow: self];
    [preferencesController setDelegate: self];
    
    //
    [[preferencesController window] makeKeyWindow];
}

//starts a quit and kills/terminates the application
-(void)doQuit: (id) menuItem
{
    //quits the application immediatly
    [NSApp terminate: self];
}

@end
