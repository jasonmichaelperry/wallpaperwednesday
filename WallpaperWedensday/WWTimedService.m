//
//  WWTimedService.m
//  WallpaperWednesday
//
//  Created by Jason Michael Perry on 10/28/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import "WWTimedService.h"

@implementation WWTimedService

//
@synthesize delegate;

//
- (void) startTimerWithInterval: (NSTimeInterval) interval
{
    //
    updateWallpaperTimer = [NSTimer scheduledTimerWithTimeInterval: interval target: self selector: @selector(timerDidFinishInterval:) userInfo: nil repeats: YES];
    
    //
    [[NSRunLoop currentRunLoop] addTimer: updateWallpaperTimer forMode: NSDefaultRunLoopMode];
}

//
- (void) timerDidFinishInterval: (NSTimer *) timer
{
    //
    [delegate selectRandomWallpaper];
}

//
- (void) stopTimer
{
    //
    [updateWallpaperTimer invalidate];
}

@end
