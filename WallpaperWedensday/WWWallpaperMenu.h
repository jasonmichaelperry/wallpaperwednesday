//
//  WWWallpaperMenuItem.h
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/26/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WWWallpaperDelegate.h"
#import "WWWallpaperItemView.h"

@interface WWWallpaperMenu : NSView
{
    //id <WWWallpaperDelegate> delegate;
    
    //array of wallpapers
    NSArray *wallpapers;
    
    //tracking information
    int current;
    
    //view elements
    NSButton *reload;
    NSButton *backButton;
    NSButton *nextButton;
    WWWallpaperItemView *itemView1;
    WWWallpaperItemView *itemView2;
    NSProgressIndicator *progressIndicator;
    NSTextView *noWallpaperText;
}

//
@property(nonatomic,weak) id<WWWallpaperDelegate> delegate;

//
- (void) setWallpapers: (NSArray *) theWallpapers;
- (void) removeDisplay;
- (void) buildDisplay;
- (void) doNext: (id) button;
- (void) doBack: (id) button;
- (void) doReload: (id) button;

@end
