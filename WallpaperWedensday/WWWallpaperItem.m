//
//  WWWallpaperItem.m
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/26/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import "WWWallpaperItem.h"

@implementation WWWallpaperItem

@synthesize name, author, description, previewImageURL, imageURL, webURL, category, localPath;

/**** INITILIZATION *****/

//
- (id)initWithName: (NSString *) theName andAuthor: (NSString *) theAuthor andDescription: (NSString *) theDescription andPreviewURL: (NSURL *) thePreviewUrl andURL: (NSURL *) theUrl andWebURL: (NSURL *) theWebUrl forCategory: (NSString *) theCategory
{
    //
    self = [super init];

    //
    if (self)
    {
        //
        name = theName;
        author = theAuthor;
        description = theDescription;
        previewImageURL = thePreviewUrl;
        imageURL = theUrl;
        webURL = theWebUrl;
        category = theCategory;
    }
    
    //
    return self;
}

/**** IMAGE KIT *****/

//
- (NSString *) imageRepresentationType
{
    //
    return IKImageBrowserNSURLRepresentationType;
}

//
- (id) imageRepresentation
{
    //
    return imageURL;
}

//
- (NSString *) imageUID
{
    //
    return name;
}

//
- (NSString *) imageTitle
{
	return name;
}

//
- (NSString *) imageSubtitle
{
    return author;
}

/**** CODING *****/

//
- (void) encodeWithCoder:(NSCoder *)aCoder
{
    //encode each element
    [aCoder encodeObject: name forKey: @"wwi_name"];
    [aCoder encodeObject: author forKey: @"wwi_author"];
    [aCoder encodeObject: description forKey: @"wwi_description"];
    [aCoder encodeObject: previewImageURL forKey: @"wwi_previewUrl"];
    [aCoder encodeObject: imageURL forKey: @"wwi_imageUrl"];
    [aCoder encodeObject: localPath forKey: @"wwi_localPath"];
}

//
-(id) initWithCoder:(NSCoder *)aDecoder
{
    //
    self = [super init];
    
    //
    if( self )
    {
        //decode each element
        [self setName: [aDecoder decodeObjectForKey: @"wwi_name"]];
        [self setAuthor: [aDecoder decodeObjectForKey: @"wwi_author"]];
        [self setDescription: [aDecoder decodeObjectForKey: @"wwi_description"]];
        [self setPreviewImageURL: [aDecoder decodeObjectForKey: @"wwi_previewUrl"]];
        [self setImageURL: [aDecoder decodeObjectForKey: @"wwi_imageUrl"]];
        [self setLocalPath: [aDecoder decodeObjectForKey: @"wwi_localPath"]];
    }
    
    //
    return self;
}

@end
