//
//  WWWallpaperMenuItemDelegate.h
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/26/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WWWallpaperItem.h"

@protocol WWWallpaperDelegate

//
-(void) reloadWallpapers;
-(void) selectWallpaper: (WWWallpaperItem *) theWallpaper;
-(void) selectRandomWallpaper;

@end
