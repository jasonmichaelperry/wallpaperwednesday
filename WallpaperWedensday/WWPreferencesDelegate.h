//
//  WWPreferencesDelegate.h
//  WallpaperWednesday
//
//  Created by Jason Michael Perry on 10/28/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WWPreferencesDelegate <NSObject>

- (void) preferencesDidChange;

@end
