//
//  WWPreferencesWindowController.m
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/27/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import "WWPreferencesWindowController.h"

@interface WWPreferencesWindowController ()

@end

@implementation WWPreferencesWindowController

/**** INITILIZATION *****/

//
@synthesize delegate, settingsView, aboutView, intervalSlider, intervalText, autoStartBox, aboutTextView;

//
- (id)init
{
    //
    self = [super initWithWindowNibName:@"PreferencesWindow" owner: self];

    //
    if (self) {
        // initializations
    }
    
    //
    return self;
}

//
- (id)initWithWindow:(NSWindow *)window
{
    //
    self = [super initWithWindow:window];
    
    //
    if (self)
    {
        // Initialization code here.
    }
    
    //
    return self;
}

//
- (void)windowDidLoad
{
    //
    [super windowDidLoad];
    
    //allows access to any default for this user
    NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
    
    //
    NSInteger intervalValue = [(NSString *)[userSettings objectForKey: @"wwp_interval"] integerValue];
    NSInteger autoStartValue = [(NSString *)[userSettings objectForKey: @"wwp_autoStart"] integerValue];
    
    //hide the initial view and only show the current selection
    [settingsView setHidden: NO];
    [aboutView setHidden: YES];
    
    //
    [intervalSlider setIntegerValue: intervalValue];
    [autoStartBox setState: autoStartValue];
    
    //
    [self setIntervalSliderValue: [intervalSlider integerValue]];
    
    //loads the contents of the about.rtf file
    [aboutTextView readRTFDFromFile: [[NSBundle mainBundle] pathForResource:@"About" ofType:@"rtf"]];
}

//
- (void) setIntervalSliderValue: (NSInteger) value
{
    switch ( value )
    {
        case 1:
            [intervalText setStringValue: @"Every 15 Minutes"];
            break;
            
        case 2:
            [intervalText setStringValue: @"Every 30 Minutes"];
            break;
            
        case 3:
            [intervalText setStringValue: @"Hourly"];
            break;
            
        case 4:
            [intervalText setStringValue: @"Daily"];
            break;
            
        case 5:
            [intervalText setStringValue: @"Weekly"];
            
            break;
        case 6:
            [intervalText setStringValue: @"Never"];
            break;
    }
}


/**** CONTROL ACTIONS *****/

//shows settings relative for the app
-(IBAction) doShowSettings: (id)sender
{
    [settingsView setHidden: NO];
    [aboutView setHidden: YES];
}

//Helps layout and show information about the application
-(IBAction) doShowAbout: (id)sender
{
    [settingsView setHidden: YES];
    [aboutView setHidden: NO];
}

//
-(IBAction) doSliderIntervalChange: (id)sender
{
    //
    [self setIntervalSliderValue: [sender integerValue]];
    
    //
    [self doSave];
}

//
-(IBAction) doToggleAutoStart: (id)sender
{
    //makes this no longer auto login
    if( [autoStartBox state] == NSOffState )
        [WWServiceLoader deleteAppFromLoginItem];
    
    //adds it as an autologin item
    else
        [WWServiceLoader addAppAsLoginItem];
    
    //
    [self doSave];
}

//
-(IBAction) doToggleCurrentOnly: (id)sender
{
    //
    [self doSave];
}

//after any control is manipluated in anyway save settings
//so they are stored for all enternity
-(IBAction) doSave
{
    //allows access to any default for this user
    NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
    
    //seralize the selected values so it reflects next run
    [userSettings setObject: [NSString stringWithFormat: @"%li", (long)[intervalSlider integerValue]] forKey: @"wwp_interval"];
    [userSettings setObject: [NSString stringWithFormat: @"%li", [autoStartBox state]] forKey: @"wwp_autoStart"];
    
    //alert that our preferences have indeed changed
    [delegate preferencesDidChange];

}

@end
