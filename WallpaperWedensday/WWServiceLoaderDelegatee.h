//
//  WWServiceLoaderDelegatee.h
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/26/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol WWServiceLoaderDelegatee

//
-(void) loadCompleteWithData: (NSArray *) theWallpapers fromService: (id) theService;
-(void) loadDidFailWithError: (NSError *) error;

@end
