//
//  WWPreferencesWindowController.h
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/27/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WWPreferencesDelegate.h"
#import "WWServiceLoader.h"

@interface WWPreferencesWindowController : NSWindowController
{
    //innner views to display settings and options
    IBOutlet NSView *settingsView;
    IBOutlet NSView *aboutView;
    
    //controls within those views to activate
    IBOutlet NSTextField *intervalText;
    IBOutlet NSSlider *intervalSlider;
    IBOutlet NSButton *autoStartBox;
    IBOutlet NSTextView *aboutTextView;
}

//
@property(nonatomic,weak) id<WWPreferencesDelegate> delegate;

//
@property(nonatomic,retain) NSView *settingsView;
@property(nonatomic,retain) NSView *aboutView;
@property(nonatomic,retain) NSTextField *intervalText;
@property(nonatomic,retain) NSSlider *intervalSlider;
@property(nonatomic,retain) NSButton *autoStartBox;
@property(nonatomic,retain) NSTextView *aboutTextView;

//
- (void) setIntervalSliderValue: (NSInteger) value;

//
-(IBAction) doShowSettings: (id)sender;
-(IBAction) doShowAbout: (id)sender;

//
-(IBAction) doSliderIntervalChange: (id)sender;
-(IBAction) doToggleAutoStart: (id)sender;
-(IBAction) doToggleCurrentOnly: (id)sender;

@end
