//
//  WWServiceLoader.m
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/26/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import "WWServiceLoader.h"

@implementation WWServiceLoader

@synthesize delegate;

//
- (id)init
{
    //
    self = [super init];
    
    //
    if (self)
    {
        //location of the remote service... maybe this should be saved somewhere and not hardcoded?
        serviceLocation = [NSURL URLWithString: @"http://services.squarebrace.com/ww/wallpapers.json"];
    }
    
    //
    return self;
}

//
-(void)loadFrom: (NSInteger) start withRange: (NSInteger) range
{
    //stores in errors in processing the JSON data feed
    NSError *error;
    
    //a mutable array of wallpaper items
    NSMutableArray *wallpaper = [[NSMutableArray alloc] init];
    
    //represents the JSON data at the designated location
    NSData *wallpaperData = [NSData dataWithContentsOfURL: serviceLocation];
    
    //nothing inside the
    if( wallpaperData == nil )
    {
        //should call a delegate function to report an error
        [delegate loadDidFailWithError: nil];
        
        //
        return;
    }
    
    //parses the data object into JSON
    NSDictionary *wallpaperDict = [NSJSONSerialization JSONObjectWithData: wallpaperData options: kNilOptions error:&error];
    
    //did we have an error?
    if( error )
    {
        //should call a delegate function to report an error
        [delegate loadDidFailWithError: error];
        
        //kill any other processing
        return;
    }
    
    //gets the container with multiple wallpaper items
    NSArray *wallpaperResults = [wallpaperDict objectForKey: @"wallpapers"];
    
    //iterate through each wallpaper item and save the result data into our mutable array
    for( int i=0; i<[wallpaperResults count]; i++ )
    {
        //get the object at this location
        NSDictionary *wi = [wallpaperResults objectAtIndex: i];
        
        //need to add a location for the description as welll
        //create a new element using this and add it to the results to return
        [wallpaper addObject: [[WWWallpaperItem alloc] initWithName: [wi objectForKey:@"name"] andAuthor: [wi objectForKey:@"author"] andDescription: [wi objectForKey:@"description"] andPreviewURL:[NSURL URLWithString: [wi objectForKey:@"previewImageURL"]] andURL:[NSURL URLWithString: [wi objectForKey:@"imageURL"]] andWebURL: [NSURL URLWithString: [wi objectForKey:@"websiteURL"]] forCategory: [wi objectForKey:@"category"]]];
    }
    
    //alerts us that the data has been loaded succesfully
    [delegate loadCompleteWithData: wallpaper fromService: self];
}

//
+(BOOL) canAccessService
{
    //
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    //
    const char *host = [@"http://wwww.nola.com/" cStringUsingEncoding: NSASCIIStringEncoding];
    
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    //SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithName( NULL, host );
    SCNetworkReachabilityFlags flags;
    BOOL gotFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    if (!gotFlags) {
        return NO;
    }
    BOOL isReachable = flags & kSCNetworkReachabilityFlagsReachable;
    
    BOOL noConnectionRequired = !(flags & kSCNetworkReachabilityFlagsConnectionRequired);
    //if ((flags & kSCNetworkReachabilityFlagsReachable)) {
        //noConnectionRequired = YES;
    //}
    
    return (isReachable && noConnectionRequired) ? YES : NO;
}

//
+(NSMutableArray *) deseralizeCurrentWallpapers
{
    //
    NSMutableArray *currentWallpapers = [[NSMutableArray alloc] init];
    
    //allows access to any default for this user
    NSUserDefaults *userSettings = [NSUserDefaults standardUserDefaults];
    
    //retreive whatever wallpaper is currently being displayed
    NSData *encodedCurrentWallpapers = [userSettings objectForKey: @"ww_currents"];
    
    //unencoded representation of the current wallpapers
    if( encodedCurrentWallpapers )
    {
        //
        NSArray *storedArray = [NSKeyedUnarchiver unarchiveObjectWithData: encodedCurrentWallpapers];
        
        //
        [currentWallpapers addObjectsFromArray: storedArray];
    }
    
    //
    return currentWallpapers;
}

//
+(void) serializeCurrentWallpapers: (NSArray *) theWallpapers
{
    [[NSUserDefaults standardUserDefaults] setObject: [NSKeyedArchiver archivedDataWithRootObject: theWallpapers] forKey:@"ww_currents"];
}

//
+(NSURL *) saveImage: (NSURL *) remotePath notDeleting: (NSArray *) theWallpapers
{
    //generates a random value
    int randomValue = rand() * 10000;
    
    //any errors from the call
    NSError *writeError;
    
    //the filename of the asset to save this as
    NSString *fileName = [remotePath lastPathComponent];
    
    //append the random value
    fileName = [NSString stringWithFormat: @"%i-%@", randomValue, fileName];
    
    //get the data for the given URL
    NSData *remoteData = [[NSData alloc] initWithContentsOfURL: remotePath];
    
    //storage paths
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex: 0] stringByAppendingPathComponent: @"WallpaperWednesday/wallpaper"];
    
    //create a file manager to check the designated location
    NSFileManager *fileManager= [NSFileManager defaultManager];
    
    //Just in case create the directory if its not there already
    [fileManager createDirectoryAtPath: filePath withIntermediateDirectories: YES attributes: nil error: NULL];
    
    //get current directory contents
    NSArray *fileArray = [fileManager contentsOfDirectoryAtPath: filePath error: &writeError];
    
    //iterate through each items and deletes
    for (NSString *filename in fileArray)
    {
        //
        BOOL found = NO;
        
        for( int i = 0; i < [theWallpapers count]; i++ )
        {
            //
            if( [filename isEqualToString: [[[theWallpapers objectAtIndex:i] localPath] lastPathComponent]] )
            {
                found = YES;
            }

        }
        
        //if found do not delete
        if( found ) continue;
        
        //removes all items previously at path so only new image is stored
        [fileManager removeItemAtPath: [filePath stringByAppendingPathComponent:filename] error: &writeError];
    }

    //attaches the filename to the end of the item
    filePath = [filePath stringByAppendingPathComponent: fileName];
    
    //save it to disk
    [remoteData writeToFile: filePath options: NSDataWritingAtomic error: &writeError];
    
    //did it fail?
    if( writeError )
    {
        return nil;
    }
    
    //return the path to the local file asset
    return [NSURL fileURLWithPath: filePath];
}

//
+ (NSImage*) resizeImage:(NSImage*)sourceImage size:(NSSize)size
{
    //
    NSRect targetFrame = NSMakeRect(0, 0, size.width, size.height);
    NSImage* targetImage = nil;
    NSImageRep *sourceImageRep =
    [sourceImage bestRepresentationForRect:targetFrame context:nil hints:nil];
    //
    targetImage = [[NSImage alloc] initWithSize:size];
    
    //
    [targetImage lockFocus];
    [sourceImageRep drawInRect: targetFrame];
    [targetImage unlockFocus];
 
    //
    return targetImage;
}

//
+(void) addAppAsLoginItem
{
	NSString * appPath = [[NSBundle mainBundle] bundlePath];
    
	// This will retrieve the path for the application
	// For example, /Applications/test.app
	CFURLRef url = (__bridge CFURLRef)[NSURL fileURLWithPath:appPath];
    
	// Create a reference to the shared file list.
    // We are adding it to the current user only.
    // If we want to add it all users, use
    // kLSSharedFileListGlobalLoginItems instead of
    //kLSSharedFileListSessionLoginItems
	LSSharedFileListRef loginItems = LSSharedFileListCreate(NULL,
                                                            kLSSharedFileListSessionLoginItems, NULL);
	if (loginItems) {
		//Insert an item to the list.
		LSSharedFileListItemRef item = LSSharedFileListInsertItemURL(loginItems,
                                                                     kLSSharedFileListItemLast, NULL, NULL,
                                                                     url, NULL, NULL);
		if (item){
			CFRelease(item);
        }
	}
    
	CFRelease(loginItems);
}

//
+(void) deleteAppFromLoginItem
{
	NSString * appPath = [[NSBundle mainBundle] bundlePath];
    
	// This will retrieve the path for the application
	// For example, /Applications/test.app
	CFURLRef url = (__bridge CFURLRef)[NSURL fileURLWithPath:appPath];
    
	// Create a reference to the shared file list.
	LSSharedFileListRef loginItems = LSSharedFileListCreate(NULL,
                                                            kLSSharedFileListSessionLoginItems, NULL);
    
	if (loginItems) {
		UInt32 seedValue;
		//Retrieve the list of Login Items and cast them to
		// a NSArray so that it will be easier to iterate.
		NSArray  *loginItemsArray = (__bridge NSArray *)LSSharedFileListCopySnapshot(loginItems, &seedValue);
        
        //
		for( int i = 0 ; i< [loginItemsArray count]; i++){
			LSSharedFileListItemRef itemRef = (__bridge LSSharedFileListItemRef)[loginItemsArray
                                                                        objectAtIndex:i];
			//Resolve the item with URL
			if (LSSharedFileListItemResolve(itemRef, 0, (CFURLRef*) &url, NULL) == noErr) {
				NSString * urlPath = [(__bridge NSURL*)url path];
				if ([urlPath compare:appPath] == NSOrderedSame){
					LSSharedFileListItemRemove(loginItems,itemRef);
				}
			}
		}
	}
}

@end
