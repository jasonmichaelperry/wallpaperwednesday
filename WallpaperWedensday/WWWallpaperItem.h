//
//  WWWallpaperItem.h
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/26/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Quartz/Quartz.h>

@interface WWWallpaperItem : NSObject<NSCoding>
{
    //
    NSString *name;
    NSString *author;
    NSString *description;
    NSURL *previewImageURL;
    NSURL *imageURL;
    NSURL *webURL;
    NSString *category;
    NSURL *localPath;
}

//
@property(nonatomic,retain) NSString *name;
@property(nonatomic,retain) NSString *author;
@property(nonatomic,retain) NSString *description;
@property(nonatomic,retain) NSURL *previewImageURL;
@property(nonatomic,retain) NSURL *imageURL;
@property(nonatomic,retain) NSURL *webURL;
@property(nonatomic,retain) NSString *category;
@property(nonatomic,retain) NSURL *localPath;

//
- (id) initWithName: (NSString *) theName andAuthor: (NSString *) theAuthor andDescription: (NSString *) theDescription andPreviewURL: (NSURL *) thePreviewUrl andURL: (NSURL *) theUrl  andWebURL: (NSURL *) theWebUrl forCategory: (NSString *) theCategory;

//
- (NSString *) imageRepresentationType;
- (id) imageRepresentation;
- (NSString *) imageUID;
- (NSString *) imageTitle;
- (NSString *) imageSubtitle;

@end
