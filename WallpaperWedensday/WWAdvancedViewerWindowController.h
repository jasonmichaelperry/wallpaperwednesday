//
//  WWAdvancedViewerWindowController.h
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/27/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Quartz/Quartz.h>
#import "WWWallpaperItemView.h"
#import "WWWallpaperDelegate.h"

@interface WWAdvancedViewerWindowController : NSWindowController
{
    //
    IBOutlet NSArray *wallpapers;
    
    //
    IBOutlet IKImageBrowserView *imageBrowser;
    IBOutlet NSSlider *zoomSlider;
}

//
@property(nonatomic,weak) id<WWWallpaperDelegate> delegate;

//
@property(nonatomic,retain) NSSlider *zoomSlider;
@property(nonatomic,retain) IKImageBrowserView *imageBrowser;

//
-(void) setWallpapers: (NSArray *) theWallpapers;

//
-(int) numberOfItemsInImageBrowser:(IKImageBrowserView *) view;
-(id) imageBrowser:(IKImageBrowserView *) view itemAtIndex:(int) index;
-(void) imageBrowser:(IKImageBrowserView *) aBrowser cellWasDoubleClickedAtIndex:(NSUInteger) index;

//
-(IBAction) doSliderMove: (id) slider;

@end
