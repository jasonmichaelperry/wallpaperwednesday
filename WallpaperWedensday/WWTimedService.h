//
//  WWTimedService.h
//  WallpaperWednesday
//
//  Created by Jason Michael Perry on 10/28/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WWWallpaperDelegate.h"
#import "WWWallpaperItem.h"

@interface WWTimedService : NSObject
{
    //
    NSTimer *updateWallpaperTimer;
}

//
@property(nonatomic,weak) id<WWWallpaperDelegate> delegate;

//
- (void) startTimerWithInterval: (NSTimeInterval) interval;
- (void) timerDidFinishInterval: (NSTimer *) timer;
- (void) stopTimer;

@end
