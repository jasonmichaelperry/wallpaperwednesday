//
//  WWWallpaperMenuItem.m
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/26/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import "WWWallpaperMenu.h"

@implementation WWWallpaperMenu

@synthesize delegate;

/**** INITILIZATIONS *****/

//handles initial display
- (id)initWithFrame:(NSRect)frame
{
    //
    self = [super initWithFrame:frame];
    
    //
    if (self) {
        
        //
        //noWallpaperText = [[NSTextView alloc] ini]
        
        //
        [self becomeFirstResponder];
        [self setWantsRestingTouches: YES];
        [self setAcceptsTouchEvents: YES];
        
        //
        reload = [[NSButton alloc] initWithFrame: NSMakeRect(frame.size.width-20, frame.size.height-20, 20, 20)];
        //[reload setTitle: @"r"];
        //[reload setAlternateTitle: @"r"];
        [reload setImage: [NSImage imageNamed: @"RefreshButton"]];
        [reload setAlternateImage: [NSImage imageNamed: @"RefreshButton"]];
        [reload setBordered: NO];
        [reload setTransparent: NO];
        [reload setTarget: self];
        [reload setAction: @selector(doReload:)];
        [reload setButtonType:NSMomentaryChangeButton];
        
        //
        backButton = [[NSButton alloc] initWithFrame: NSMakeRect(0, 5, 20, frame.size.height-20)];
        //[backButton setTitle: @"<"];
        //[backButton setAlternateTitle: @"<"];
        [backButton setImage: [NSImage imageNamed: @"BackButton"]];
        [backButton setAlternateImage: [NSImage imageNamed: @"BackButton"]];
        [backButton setBordered: NO];
        [backButton setTransparent: NO];
        [backButton setTarget: self];
        [backButton setAction: @selector(doBack:)];
        [backButton setEnabled: NO];
        [backButton setButtonType:NSMomentaryChangeButton];
        [(NSButtonCell *)[backButton cell] setImageDimsWhenDisabled: NO];
        
        //
        nextButton = [[NSButton alloc] initWithFrame: NSMakeRect(frame.size.width-20, 5, 20, frame.size.height-20)];
        //[nextButton setTitle: @">"];
        //[nextButton setAlternateTitle: @">"];
        [nextButton setImage: [NSImage imageNamed: @"NextButton"]];
        [nextButton setAlternateImage: [NSImage imageNamed: @"NextButton"]];
        [nextButton setBordered: NO];
        [nextButton setTransparent: NO];
        [nextButton setTarget: self];
        [nextButton setAction: @selector(doNext:)];
        [nextButton setEnabled: NO];
        [nextButton setButtonType:NSMomentaryChangeButton];
        [(NSButtonCell *)[nextButton cell] setImageDimsWhenDisabled: NO];
        
        //
        itemView1 = [[WWWallpaperItemView alloc] initWithFrame: NSMakeRect(25, 0, 150, 100)];
        [itemView1 setEnabled: false];
        
        //
        itemView2 = [[WWWallpaperItemView alloc] initWithFrame: NSMakeRect(225, 0, 150, 100)];
        [itemView2 setEnabled: false];
        
        //creates the progress indicator as our initial display
        progressIndicator = [[NSProgressIndicator alloc] initWithFrame:NSMakeRect(frame.size.width/2-10, frame.size.height/2-10, 20, 20)];
        [progressIndicator setStyle: NSProgressIndicatorSpinningStyle];
        [progressIndicator setUsesThreadedAnimation: YES];
        [progressIndicator setIndeterminate: YES];
        [progressIndicator startAnimation: self];
        
        //adds elements to the view
        [self addSubview: itemView1];
        [self addSubview: itemView2];
        [self addSubview: backButton];
        [self addSubview: nextButton];
        [self addSubview: progressIndicator];
        //[self addSubview: reload];    //i think this should happen magically
    }
    
    //
    return self;
}

//
- (void)drawRect: (NSRect)dirtyRect
{
    //
    [super drawRect:dirtyRect];
    
    //enlarges the size to handle the gap above the top of the menu
    NSRect fullBounds = [self bounds];
    fullBounds.origin.y -= 6;
    fullBounds.size.height += 12;

    //
    [[NSColor selectedMenuItemColor] setFill];
    
    //
    [[NSBezierPath bezierPathWithRect:fullBounds] setClip];
    
    
    //
    //NSGradient* gradient = [[NSGradient alloc] initWithStartingColor: [NSColor selectedMenuItemColor] endingColor: [NSColor selectedMenuItemColor]];
    //[gradient drawInRect: fullBounds angle: 270];
    
    //NSRectFill(dirtyRect);
    //NSRectFill(fullBounds);
}

//
- (void) startAnimation
{
    //start the animation of the instance
    [progressIndicator startAnimation: self];
}

/**** DISPLAY CONSTRUCTION *****/

//sets the wallpapers to display
- (void)setWallpapers: (NSArray *) theWallpapers
{
    //remove the indicator to represent a new set of wallpapers
    [progressIndicator stopAnimation: nil];
    [progressIndicator removeFromSuperview];
    
    //removes all items from display
    [self removeDisplay];
    
    //applies the loaded wallpappers for rendering
    wallpapers = theWallpapers;
    
    //do we have any wallpaeprs?
    if( [wallpapers count] == 0 )
    {
        //
        [backButton setEnabled: NO];
        [nextButton setEnabled: NO];
        
        //We have nothing to show? Maybe a message stating that
        //We also need to hide all elements previously displayed
    }
    
    //we have wallpapers so render the display
    else
    {
        //
        [backButton setEnabled: YES];
        [nextButton setEnabled: YES];
        
        //render a display of the wallpaper items
        [self buildDisplay];
    }
    
}

//destroys all items current being rendered
- (void) removeDisplay
{

}

//creates the existing display
- (void) buildDisplay
{
    //reset the initial positioning of these items
    [itemView1 setFrameOrigin: NSMakePoint(25, 0)];
    [itemView2 setFrameOrigin: NSMakePoint(225, 0)];
    
    //
    [itemView1 setDelegate: delegate];
    [itemView1 displayWithWallpaperItem: [wallpapers objectAtIndex:0]];
    [itemView1 setEnabled: YES];
    
    //
    current = 0;
}

/**** ACTIONS *****/

//did swipe left or right on the image
- (void) swipeWithEvent:(NSEvent *)event
{
    //move back an item on swipe left
    if( [event deltaX] < 0 )
        [self doBack: nil];
    
    //move to the next item on swipe
    if( [event deltaX] > 0 )
        [self doNext: nil];
}


//goes forward one wallpaper
- (void) doNext: (id) button
{
    //
    current = (current+1 == [wallpapers count] ) ? 0 : current + 1;
    
    //
    [itemView2 setFrameOrigin: NSMakePoint(225, 0)];
    [itemView2 setDelegate: delegate];
    [itemView2 displayWithWallpaperItem: [wallpapers objectAtIndex: current]];

    //non are enabled until visible
    [itemView1 setEnabled: NO];
    [itemView2 setEnabled: NO];
    
    //disables button until animation completes
    [backButton setEnabled: NO];
    [nextButton setEnabled: NO];
    
    //
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setCompletionHandler:^{

        //enable for selection after animation
        [itemView2 setEnabled: YES];
        
        //swaps the items so next move still works
        WWWallpaperItemView *buffer = itemView1;
        itemView1 = itemView2;
        itemView2 = buffer;
        
        //now move this item to the original spot®
        //enables button after animation completes
        [backButton setEnabled: YES];
        [nextButton setEnabled: YES];
    }];
    [[NSAnimationContext currentContext] setDuration: 0.25f];
    [[itemView1 animator] setFrameOrigin: NSMakePoint(itemView1.frame.origin.x-200, 0)];
    [[itemView2 animator] setFrameOrigin: NSMakePoint(itemView2.frame.origin.x-200, 0)];
    [NSAnimationContext endGrouping];
}

//goes back one wallpaper
- (void) doBack: (id) button
{
    //
    current = (current-1 < 0 ) ? ((int)[wallpapers count])-1 : current - 1;
    
    //
    [itemView1 setFrameOrigin: NSMakePoint(-200, 0)];
    [itemView1 setDelegate: delegate];
    [itemView1 displayWithWallpaperItem: [wallpapers objectAtIndex: current]];
    
    //if the value is
    
    //non are enabled until visible
    [itemView1 setEnabled: NO];
    [itemView2 setEnabled: NO];
    
    //disables button until animation completes
    [backButton setEnabled: NO];
    [nextButton setEnabled: NO];
    
    //
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setCompletionHandler:^{
        
        //enable for selection after animation
        [itemView1 setEnabled: YES];
        
        //swaps the items so next move still works
        WWWallpaperItemView *buffer = itemView2;
        itemView2 = itemView1;
        itemView1 = buffer;
        
        //now move this item to the original spot
        [itemView1 setFrameOrigin: NSMakePoint(-225, 0)];
        
        //enables button after animation completes
        [backButton setEnabled: YES];
        [nextButton setEnabled: YES];
    }];
    [[NSAnimationContext currentContext] setDuration: 0.25f];
    [[itemView1 animator] setFrameOrigin: NSMakePoint(itemView1.frame.origin.x+225, 0)];
    [[itemView2 animator] setFrameOrigin: NSMakePoint(itemView2.frame.origin.x+200, 0)];
    [NSAnimationContext endGrouping];
}

//triggers a reload of all assets
- (void) doReload: (id) button
{
    //alerts delegate that wallpapers should be reloaded
    [delegate reloadWallpapers];
}


@end
