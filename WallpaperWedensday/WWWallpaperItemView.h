//
//  WWWallpaperItemView.h
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/27/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WWWallpaperDelegate.h"
#import "WWWallpaperItem.h"
#import "WWServiceLoader.h"

@interface WWWallpaperItemView : NSView
{
    //
    NSButton *previewImage;
    
    //
    WWWallpaperItem *wallpaerItem;
}

//
@property(nonatomic,weak) id<WWWallpaperDelegate> delegate;
@property(nonatomic,retain) WWWallpaperItem *wallpaperItem;

//
- (void) doSelect: (NSImageView *) imageView;
- (void) displayWithWallpaperItem: (WWWallpaperItem *) theWallpaperItem;
- (void) setEnabled: (BOOL) isEnabled;

@end
