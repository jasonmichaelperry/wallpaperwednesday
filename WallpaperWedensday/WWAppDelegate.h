//
//  WWAppDelegate.h
//  WallpaperWedensday
//
//  Created by Jason Michael Perry on 10/26/13.
//  Copyright (c) 2013 Jason Michael Perry. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WWServiceLoader.h"
#import "WWWallpaperMenu.h"
#import "WWWallpaperDelegate.h"
#import "WWPreferencesDelegate.h"
#import "WWServiceLoaderDelegatee.h"
#import "WWWallpaperItem.h"
#import "WWPreferencesWindowController.h"
#import "WWAdvancedViewerWindowController.h"
#import "WWTimedService.h"
#import "WWCurrentWallpaperItemView.h"

@interface WWAppDelegate : NSObject <NSApplicationDelegate,WWServiceLoaderDelegatee,WWWallpaperDelegate,WWPreferencesDelegate>
{
    //
    float timeout;
    
    //
    NSMutableArray *currentWallpapers;
    WWWallpaperItem *currentWallpaper;
    
    //
    NSArray *wallpapers;
    
    //
    NSMenu *menu;
    NSStatusItem *statusItem;
    WWWallpaperMenu *wallpaperMenuView;
    WWCurrentWallpaperItemView *currentWallpaperView;
    
    //
    NSMenuItem *currentWallpaperMenuItem;
    
    //
    WWTimedService *timedServices;
}

//
@property (nonatomic, strong) WWAdvancedViewerWindowController *advancedViewerController;
@property (nonatomic, strong) WWPreferencesWindowController *preferencesController;
@property (assign) IBOutlet NSWindow *window;

//
- (void) isFirstRun;
- (void)activateStatusMenu;

//
- (void) startTimerForInterval: (NSInteger) interval;

//
- (void) loadWallpapers;
- (void) applyWallpaper:(WWWallpaperItem *)theWallpaper;

//
- (void) showNotification;

//
-(void)showAdvancedViewer: (id) menuItem;
-(void)showPreferences: (id) menuItem;
-(void)doQuit: (id) menuItem;

@end
